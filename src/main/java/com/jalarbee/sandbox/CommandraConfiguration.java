/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jalarbee.sandbox;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.stuartgunter.dropwizard.cassandra.CassandraFactory;

/**
 *
 * @author diallo
 */
public class CommandraConfiguration extends Configuration {
    
    @Valid
    @NotNull
    private CassandraFactory cassandraFactory = new CassandraFactory();

    @JsonProperty("cassandra")
    public void setCassandraFactory(CassandraFactory cassandraFactory) {
        this.cassandraFactory = cassandraFactory;
    }

    @JsonProperty("cassandra")
    public CassandraFactory getCassandraFactory() {
        return cassandraFactory;
    }
    
    
}
