package com.jalarbee.sandbox;

import com.datastax.driver.core.Cluster;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;


public class CommandraApplication  extends Application<CommandraConfiguration>{

    @Override
    public void initialize(Bootstrap<CommandraConfiguration> bootstrap) {
        
    }

    @Override
    public String getName() {
        return "Educ8or";
    }
    
    @Override
    public void run(CommandraConfiguration configuration, Environment environment) throws Exception {
        
        Cluster cluster = configuration.getCassandraFactory().build(environment);
    }
    
    public static void main(String[] args) throws Exception {
        new CommandraApplication().run(args);
    }
}
